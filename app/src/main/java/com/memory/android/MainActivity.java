package com.memory.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.memory.android.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements GameAdapter.Callback {
    private ActivityMainBinding mBinding;
    private int mGridRows;
    private int mGridColumns;
    private GameAdapter gameAdapter;
    private int mCurrentSelectedImageRes = -1;
    private int mCurrentSelectedItemPosition = -1;
    long timerInit = -1;
    long gameStartTime = -1;
    private boolean isGameOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            gameAdapter = new GameAdapter(savedInstanceState, this);
            mGridColumns = gameAdapter.getColumnsCount();
            mGridRows = gameAdapter.getRowsCount();
            timerInit = savedInstanceState.getLong(KEY.TIMER, -1);
            gameStartTime = savedInstanceState.getLong(KEY.GAME_START_TIME);
            isGameOver = savedInstanceState.getBoolean(KEY.IS_GAME_OVER);
            mCurrentSelectedItemPosition = savedInstanceState.getInt(KEY.SELECTED_POS, -1);
            mCurrentSelectedImageRes = savedInstanceState.getInt(KEY.SELECTED_IMG_RES, -1);
        } else {
            mGridRows = getIntent().getIntExtra(ARGS.ROWS, 0);
            mGridColumns = getIntent().getIntExtra(ARGS.COLUMNS, 0);
            gameAdapter = new GameAdapter(mGridRows, mGridColumns, this);
        }
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        GridLayoutManager layoutManager = new GridLayoutManager(this, mGridColumns);
        mBinding.rvGame.setLayoutManager(layoutManager);
        mBinding.rvGame.setAdapter(gameAdapter);
        mBinding.rvGame.addItemDecoration(new ItemOffsetDecoration(20, 20));
        if (mCurrentSelectedItemPosition != -1)
            gameAdapter.setCurrentSelected(mCurrentSelectedItemPosition);
        if (gameStartTime == -1)
            gameStartTime = SystemClock.elapsedRealtime();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timerInit != -1 && gameStartTime != -1 && !isGameOver)
            mBinding.chronometer.setBase(timerInit);
        mBinding.chronometer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerInit = mBinding.chronometer.getBase();
        mBinding.chronometer.stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        gameAdapter.saveGameState(outState);
        outState.putInt(KEY.SELECTED_POS, mCurrentSelectedItemPosition);
        outState.putInt(KEY.SELECTED_IMG_RES, mCurrentSelectedImageRes);
        outState.putBoolean(KEY.IS_GAME_OVER, isGameOver);
        outState.putLong(KEY.TIMER, mBinding.chronometer.getBase());
        outState.putLong(KEY.GAME_START_TIME, gameStartTime);
    }

    @Override
    public void onItemClicked(int imageResId, int itemPosition) {
        if (mCurrentSelectedImageRes == -1) {
            mCurrentSelectedImageRes = imageResId;
            mCurrentSelectedItemPosition = itemPosition;
        } else {
            if (imageResId == mCurrentSelectedImageRes && itemPosition != mCurrentSelectedItemPosition) {
                //correct pair. set bg to green
                gameAdapter.showCorrect(mCurrentSelectedItemPosition, itemPosition);
            } else {
                // incorrect pair. hide both
                gameAdapter.hide(mCurrentSelectedItemPosition, itemPosition);
            }
            mCurrentSelectedImageRes = -1;
            mCurrentSelectedItemPosition = -1;
        }
    }

    @Override
    public void onGameFinished() {
        mBinding.chronometer.stop();
        isGameOver = true;
        long timetaken = SystemClock.elapsedRealtime() - gameStartTime;
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra(ResultsActivity.ARGS.TIME_TAKEN_MILLIS, timetaken);
        intent.putExtra(ResultsActivity.ARGS.ROWS, mGridRows);
        intent.putExtra(ResultsActivity.ARGS.COLUMNS, mGridColumns);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        if (isGameOver) {
            super.onBackPressed();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setMessage("Do you want to exit?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();

            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
        private int mItemOffsetVertical;
        private int mItemOffsetHorizontal;

        private ItemOffsetDecoration(int itemOffsetV, int itemOffsetH) {
            mItemOffsetVertical = itemOffsetV;
            mItemOffsetHorizontal = itemOffsetH;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetIdVertical, @DimenRes int itemOffsetIdHorizontal) {
            this(context.getResources().getDimensionPixelSize(itemOffsetIdVertical), context.getResources().getDimensionPixelSize(itemOffsetIdHorizontal));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffsetHorizontal, mItemOffsetVertical, mItemOffsetHorizontal, mItemOffsetVertical);
        }
    }

    private interface KEY {
        String SELECTED_POS = "sp";
        String SELECTED_IMG_RES = "sir";
        String TIMER = "timer";
        String GAME_START_TIME = "gst";
        String IS_GAME_OVER = "igo";
    }

    public interface ARGS {
        String ROWS = "rows";
        String COLUMNS = "cols";
    }
}
