package com.memory.android;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.memory.android.databinding.ActivityResultsBinding;

public class ResultsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityResultsBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_results);
        binding.tvGrid.setText(getIntent().getIntExtra(ARGS.ROWS,0) +"X" + getIntent().getIntExtra(ARGS.COLUMNS,0));

        long timeTakenMillis = getIntent().getLongExtra(ARGS.TIME_TAKEN_MILLIS,0);

        int mins = (int) (timeTakenMillis/1000/60);
        int secs = (int) ((timeTakenMillis/1000) % 60);

        binding.tvTime.setText(mins+":"+secs);
    }

    public interface ARGS {
        String ROWS = "rows";
        String COLUMNS = "cols";
        String TIME_TAKEN_MILLIS = "ttm";
    }
}
