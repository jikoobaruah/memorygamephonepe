package com.memory.android;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.memory.android.databinding.ItemGridBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameHolder> {
    int[] images = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5};
    private Callback mCallback;
    private  int mGridRows;
    private  int mGridColumns;
    private ArrayList<Integer> imagesList;
    private ArrayList<Integer> correctPositions;

    public GameAdapter(int rows, int columns, Callback callback) {
        mGridRows = rows;
        mGridColumns = columns;
        mCallback = callback;
        imagesList = new ArrayList<>(mGridRows * mGridColumns);
        correctPositions = new ArrayList<>();
        fillList();
    }

    public GameAdapter(Bundle savedState, Callback callback){
        mGridRows = savedState.getInt(KEY.ROWS);
        mGridColumns = savedState.getInt(KEY.COLUMNS);
        correctPositions = savedState.getIntegerArrayList(KEY.CORRECT_POSITIONS);
        imagesList = savedState.getIntegerArrayList(KEY.IMAGES_LIST);
        mCallback = callback;
    }

    private void fillList() {
        imagesList.clear();
        int fillCount = 0;
        int imageIndex;
        while (fillCount < mGridRows * mGridColumns) {
            imageIndex = new Random().nextInt(images.length);
            imagesList.add(imageIndex);
            imagesList.add(imageIndex);
            fillCount = fillCount + 2;
        }
        Collections.shuffle(imagesList);
    }

    @NonNull
    @Override
    public GameHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemGridBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_grid, viewGroup, false);
        return new GameHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GameHolder viewHolder, int i) {
        int imageIndex = imagesList.get(i);
        viewHolder.bindView(images[imageIndex]);
    }

    @Override
    public int getItemCount() {
        return mGridRows * mGridColumns;
    }

    public void showCorrect(int itemPosition1, int itemPosition2) {
        correctPositions.add(itemPosition1);
        correctPositions.add(itemPosition2);
        notifyItemChanged(itemPosition1);
        notifyItemChanged(itemPosition2);

        if (correctPositions.size() == getItemCount() && mCallback != null)
            mCallback.onGameFinished();
    }

    public void hide(int itemPosition1, int itemPosition2) {
        notifyItemChanged(itemPosition1);
        notifyItemChanged(itemPosition2);
    }

    public void saveGameState(Bundle outState) {
        outState.putInt(KEY.ROWS, mGridRows);
        outState.putInt(KEY.COLUMNS, mGridColumns);
        outState.putIntegerArrayList(KEY.CORRECT_POSITIONS, correctPositions);
        outState.putIntegerArrayList(KEY.IMAGES_LIST, imagesList);
    }

    public int getColumnsCount() {
        return mGridColumns;
    }

    public int getRowsCount() {
        return mGridRows;
    }

    private int mSelectedPosiiton = -1;

    public void setCurrentSelected(int selectedPosition) {
        mSelectedPosiiton = selectedPosition;
        notifyItemChanged(selectedPosition);
    }

    public class GameHolder extends RecyclerView.ViewHolder {
        private ItemGridBinding mBinding;

        public GameHolder(ItemGridBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (correctPositions.contains(getAdapterPosition()))
                        return;

                    int visibility = mBinding.img.getVisibility();
                    mBinding.img.setVisibility(visibility == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
                    if (mCallback != null)
                        mCallback.onItemClicked(images[imagesList.get(getAdapterPosition())], getAdapterPosition());
                }
            });
        }

        public void bindView(int imageResId) {
            mBinding.img.setImageResource(imageResId);
            if (correctPositions.contains(getAdapterPosition())) {
                mBinding.img.setVisibility(View.VISIBLE);
                mBinding.cv.setCardBackgroundColor(Color.GREEN);
            } else {
                if (mSelectedPosiiton ==  getAdapterPosition()){
                    mBinding.img.setVisibility(View.VISIBLE);
                    mSelectedPosiiton = -1;
                }else {
                    mBinding.img.setVisibility(View.INVISIBLE);
                }
                mBinding.cv.setCardBackgroundColor(Color.RED);
            }

        }
    }

    public interface Callback {
        void onItemClicked(int imageResId, int itemPosition);

        void onGameFinished();
    }

    public interface KEY {
        String ROWS = "rows";
        String COLUMNS = "cols";
        String CORRECT_POSITIONS = "cp";
        String IMAGES_LIST = "il";
    }
}