package com.memory.android;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.memory.android.databinding.ActivityGameSelectionBinding;

public class GameSelectionActivity extends AppCompatActivity {

    String[] gridSizes = {"1x2","1x4","2x2","2x3","2x4","3x2","3x4","4x4"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityGameSelectionBinding binding  =  DataBindingUtil.setContentView(this,R.layout.activity_game_selection);
        binding.rvGridSelection.setAdapter(new SelectionAdapter());
        binding.rvGridSelection.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        binding.rvGridSelection.addItemDecoration(itemDecorator);
    }

    private class SelectionAdapter extends RecyclerView.Adapter<SelectionViewHolder> {
        @NonNull
        @Override
        public SelectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_grid_size,viewGroup,false);
            return new SelectionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SelectionViewHolder viewHolder, int i) {
            viewHolder.bindView();
        }

        @Override
        public int getItemCount() {
            return gridSizes.length;
        }
    }

    private class SelectionViewHolder extends RecyclerView.ViewHolder{
        public SelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GameSelectionActivity.this,MainActivity.class);

                    String text = gridSizes[getAdapterPosition()];
                    String[] grid = text.split("x");

                    intent.putExtra(MainActivity.ARGS.ROWS, Integer.parseInt(grid[0]));
                    intent.putExtra(MainActivity.ARGS.COLUMNS, Integer.parseInt(grid[1]));
                    startActivity(intent);
                }
            });
        }

        public void bindView() {
            ((TextView)itemView).setText(gridSizes[getAdapterPosition()]);
        }
    }
}
